# Where To Go From Here

So we've run into limits with the Arduino platform, because we can't really edit main.cpp and wiring.c to add our idle() and tick() calls. We could, but that's a great way to lead to problems when updates come out. We could buy a programmer to bypass the Arduino bootloader on the processor, but then your Arduino no longer functions as an Arduino(Without figuring out how to reprogram the bootloader, anyways), which I'd rather not do to your devices.

So where do we go from here? There's a lot of options for microcontrollers, ranging the gamut from cheap to expensive, simple to complex, built-to-do-one-thing to can-toast-your-bread-and-clean-your-shower-at-the-same-time.

Everybody's going to have their own preference, but the selection criteria I'm going to go with for the purposes of this tutorial set is "cheap to get started" and "powerful for the price". For these two criteria, it's hard to go wrong with ST Microelectronics' STM32 line. There are many easy-to-find development boards for it for very low prices, their parts are powerful and usually packed with features, and the programmers for their parts are easy to get, too.

So let's start building a shopping list...

## Things to shop around for

### Development Board

[See here](https://wiki.stm32duino.com/index.php?title=Blue_Pill) for pictures and some documentation.

This board goes by many names, though it's usually described as the "Blue Pill" or "STM32F103 minimum system board". It can be found on Amazon, AliExpress, and eBay, among many other vendors, usually for a good deal less than $10 each. It's usually among the first results for a search for "STM32F103", too.

If you don't have a soldering iron, try and find one with the headers pre-soldered.

This board is built around the STM32F103C8T6 processor, which has an ARM Cortex M3 core running at 72MHz, and it has either 64KB or 128KB of program space, and 20KB of RAM, so it's got plenty of resources for us to work with.

### Programmer

[See here](https://wiki.paparazziuav.org/wiki/File:Stlinkv2_clone_aluminium.jpg) for a picture.

This is a clone of the official STMicroelectronics programmer for the STM32 line. It's a little bit cheaper than the official one, and it gets the job done. There's a few other types of clone out there, but the ones that look like this I've had fairly good luck with working.

These can also be found on Amazon, AliExpress, and eBay, usually by searching for "ST-Link".

If you want to splurge for the official programmer, order it from a reputable electronics supplier like Digikey, Mouser, or Element14, not from the other places listed above. You are very unlikely to get a real one. Search for "ST-LINK/V2" on those websites to get the real one. It should look like the mostly-white one on [this page](https://www.st.com/en/development-tools/st-link-v2.html).

### Basic Electronics Kit

[Electronics Fun Kit](https://www.amazon.com/ELEGOO-Electronics-Potentiometer-tie-Points-Breadboard/dp/B01ERPEMAC/)

Easier than trying to piece together a kit of your own, this includes an assortment of very common prototyping pieces. For that particular kit, you'll also want a [power adapter](https://www.amazon.com/ELEGOO-100V-240V-Converter-Adapter-Certificate/dp/B074BRR5YN/) to go with it.

A breadboard, LEDs, buttons, resistors, and jumper wires are the "main" things to look for. Everything else is extra.

### Miscellaneous Other Things

None of the things in this section are necessarily required, but if you're making an order for electronics stuff anyways, it probably wouldn't hurt.

* [Multimeter](https://www.amazon.com/Auto-Ranging-Digital-Multimeter-Continuity/dp/B07KPBGR3R/) - Look for one that has autoranging, it'll make your life much easier.
* [Jumper wires](https://www.amazon.com/EDGELEC-Breadboard-Optional-Assorted-Multicolored/dp/B07GD2BWPY/) - These are handy for throwing together quick tests on a breadboard. Look for a set that has an assortment of at least Male-to-Male and Male-to-Female. Female-to-Female comes in handy occasionally, but less than the other two.

## Done Shopping?

Alright, let's talk a bit about these new processors while the mail ships. I mentioned earlier that the STM32 Blue Pill is based on an STM32F103C8T6, which is quite the part number. Let's break it down a little bit.

"STM32" is the shorthand name for STMicro's 32-bit processor family.

F103 is the position of this particular part in that family. The F just means that it's a Flash part, which means that it's reprogrammable. There are also P103s, which are only programmable once, which definitely isn't what we're looking for.

The 103 breaks down a bit more.

The first digit, 1, tells us this is a "mainstream" part. 0 would be "entry level", and 3 is their super fancy high-end parts. Currently there isn't a 2. Generally this tells you how fast the chip can do math. Entry-level parts can do less math than mainstream parts, which can do less math than the high-end parts.

The last two digits generally signify how many peripherals are available on that part, with higher numbers usually meaning more peripherals, and sometimes more speed to go with them.

The F103 basically runs right down the center of all of these considerations. It has a decent selection of peripherals, but not all of them. It can do some math, but not as fast as it's bigger brothers. It's got some raw processing power, but the lowest part in the F3 line can match it. But because it's average all around, it also gets used in a lot of projects, and so it's cheaper because more of them get made. Which is a win for us.

The C8 in the part number tells us that this part has 64KB of Flash onboard for program storage. It has been found that a lot of the C8 parts do actually have 128KB of Flash, thanks to the wonders of mass production, but this is in no way guaranteed, so don't expect it.

The T6 tells us both that this chip is packaged as an LQFP-48 and that it's a "normal" temperature range part. Neither of these probably mean much to you (yet!), which is okay, so don't worry about it. We're buying these already on development boards, so we don't have to worry about either of these things yet.

## Didn't you also mention some Arm Cortex something-or-other?

I did, yes. Rather than build their own processors, STMicro licenses processor technology from ARM, and just builds their stuff around the ARM technology. ARM might sound familiar, because their processor technology gets used in almost every phone currently in production. ARM is very good at making processor cores, and they've turned that into their entire business. They don't actually produce any physical chips themselves, they just sell the designs to other companies to integrate into their chips.

So STMicro buys these processor core designs from ARM, and then sticks their peripherals onto them, and sells those chips to us. In particular, there's four processor cores that STMicro uses from ARM: Cortex-M0, Cortex-M0+, Cortex-M3, and Cortex-M4.

The Cortex M0 and Cortex M0+ get used in the STM32F0 line. They're very low power, and fairly slow (With speeds reaching up to 48MHz) compared to their bigger brothers. They're not very good at math, but can still get quite a bit done for their price.

The Cortex M3 is used in the STM32F1 line, and is a major step up in processing power from the M0 and M0+, with additional math capabilities and speeds reaching up to 72MHz.

The Cortex M4 is used in the STM32F3 line, and has significantly more math capabilities. Even though it's still running at 72MHz, it can do most math in a single processor cycle, where the M0 and M3 could take hundreds of cycles.

For now, none of this really matters, because I'm picking the processor for you, but this is something to keep in the back of your mind for down the road.

## So, uh... How do we program these things, since we can't use the Arduino program?

That's a very good question. Which I'll answer in the next part, because coming from Arduino, there's a lot to cover in the world of Integrated Development Environments.