# Blink that LED

NOTE: At some point during this, you'll probably get a window that looks something like [this](https://drive.google.com/file/d/1Rx_dASPgKET4ki8PvwuPnTI5iOyLNZV8/view?usp=drivesdk). When you do, make sure the "Always save resources before launching" is checked, then click "Okay".

So the code that we have now just sets up the hardware, it doesn't actually do anything. Let's at least get it to blink the LED, first.

We're not going to set up our ideal architecture, yet, we just want to make sure that your programmer and your development board work.

In main.c, in the "USER CODE BEGIN 2" block, add a "uint32_t temp = 0;" line. In the "USER CODE BEGIN 3" block, before the } that closes the while(1) loop, add this:

```C

temp++;
if(temp == 4000000) {
    HAL_GPIO_TogglePin(GPIOC, GPIO_PIN_13);
    temp = 0;
}

```

Our processor is running at 72 MHz(Or 72 million instructions per second), so this will toggle the LED roughly every second. But wait, 4000000 != 72000000, so how is this taking 1 second, you ask? Well, by default, STM32CubeIDE does not turn on any compiler optimization, so the compiler defaults to the "slow and safe" version of everything. All in all, the loop adds up to ~18 instructions worth of time per loop, so 72000000/18 = 4000000.

But that's awful wasteful, and will only get moreso as we write more code, so let's go turn on some optimizations. In the Project menu up on the toolbar, click Properties. Expand the "C/C++ Build" menu in the window that opens, then click "Settings". In the right pane, click the "Tool Settings" tab, then expand the "MCU GCC Compiler" section, then click "Optimization, then set the "Optimization level" to "Optimize for Debug (-Og)". This leaves some extra information in the file to make it easier to debug, but does most of the optimizations to make the code efficient otherwise. Then click "Apply and Close". Then click the Build button.

Now, the loop takes up only 9 instructions worth of time per loop, so 72000000/9 = 8000000, so make sure to change the check to 8000000.

Don't worry, we'll get back to measuring time correctly soon. This just happens to be a good way to introduce compiler optimizations, since it's off by default.

Let's see this actually work, though!

# Connecting the board to the programmer

So you've got your [Blue Pill](https://photos.app.goo.gl/Sfj6fmDbVbAwdyjz5), and you've got your [programmer](https://photos.app.goo.gl/XZ8FGmyGtd8kg8Pp9). Let's get the two together.

The Blue Pill has four pins sticking out of the end, and the programmer should have come with a little four pin jumper cable. Connect one end of the jumper cable to those four pins, then flip the Blue Pill over to see the [labels on the back](https://photos.app.goo.gl/4mkfNpp5rXhXbiyK8).

The programmer has it's pinout printed on the case. In my case, they show the "keying" cutout on the connector with an extra block of white on the printed pinout, to help you orient the connections correctly. SWDIO on the programmer connects to SWO on the Blue Pill, one of the GNDs on the programmer connects to GND on the Blue Pill, SWCLK goes to SWCLK, and 3.3V goes to 3V3. Be very careful not to connect 3V3 on the Blue Pill to one of the 5.0V pins on the programmer! You WILL damage the Blue Pill! If you aren't sure that you've got it hooked up correctly, send me a few pictures and I'll be glad to double check it for you.

At this point, you should be able to plug the programmer into a USB port on your computer, and see the [red LED light up on the programmer](https://photos.app.goo.gl/m2n6N7kdPmfXwV8K6), and the [PWR LED light up on the Blue Pill](https://photos.app.goo.gl/BVx1VwFcqxJg4KfS8). If either of these do not light up, unplug the programmer IMMEDIATELY, and double check your connections. You should also hear your computer do the "device connected" sound.

Congrats, your programmer and Blue Pill are now set up for us to do some work.

# Programming the processor

Up on the toolbar, there's a button that looks like a little bug. Click that, and it should ask you how you want to run the application. Select "STM32 MCU C/C++ Application". It will also pop up with another window with a bunch of options, just click "Apply" then "Debug". After this, it might tell you that the programmer needs a firmware update. Click the update button, and then you'll get a window that looks like [this](https://drive.google.com/file/d/1uWwGgJaPqH7zF4_NFiNS0sHBrrS0Bp5c/view?usp=drivesdk). Click "Open in update mode". If the "Upgrade" button becomes clickable, great, if not, try unplugging the programmer from your computer and plugging it back in, then click the "open in update mode" button again. But click the "Upgrade" button when you can, then it will think for a bit, and should tell you it succeeded. Close the Upgrade window, then try clicking the bug button again. You'll get another "Confirm Perspective Switch" window, check "Remember my decision", then click "Switch.

Now your IDE window will [look a little bit different](https://drive.google.com/file/d/1c8YnvEFt-tWVhIQ-xxyMrj_fhAi5pLn2/view?usp=drivesdk), and have some different buttons on the toolbar. This is the "debugging" view. At this point, the program is downloaded to your processor, it's just waiting for you to tell it to run. Click the little ["play/pause" button](https://drive.google.com/file/d/10KNQ_TUkbmR7PbHItK8f94xqICWFUUhs/view?usp=drivesdk), and then look at your Blue Pill board. Next to the PWR LED is another LED, which is the one we're actually controlling. It should be [blinking](https://photos.app.goo.gl/zgDSVVvK5NMy19yK7)!

When you're done watching it blink, click the red "Terminate"/"Stop" button up on the toolbar, and the IDE will go back to editing mode.

Congrats! You've just run your first code on the STM32!

